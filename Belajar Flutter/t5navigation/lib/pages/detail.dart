import 'package:flutter/material.dart';
import 'package:t5navigation/pages/home.dart';

class Detail extends StatefulWidget {
  @override
  _DetailState createState() => _DetailState();
}

class _DetailState extends State<Detail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Halaman 2'),
      automaticallyImplyLeading: false,
      ),
      body: FlatButton(
        color: Colors.orange,
        onPressed: (){
          Navigator.of(context).pop(new MaterialPageRoute(
          builder: (BuildContext context) => Home()));
        },
        child: Text("Ke Halaman 2"),
      ),

      
    );
  }
}