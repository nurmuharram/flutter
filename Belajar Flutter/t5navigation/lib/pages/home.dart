import 'package:flutter/material.dart';
import 'package:t5navigation/pages/detail.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Halaman 1')),
      body: FlatButton(
        color:  Colors.orange,
        onPressed: (){
          Navigator.of(context).push(new MaterialPageRoute(
          builder: (BuildContext context) => Detail()));
        },
        child:  Text("Ke halaman sebelah"),
        ),
    );
  }
}