import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:t4calculator/pages/home.dart';



void main () {
  runApp(MyApp());
}


class  MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title:'Calculator',
      home: Home(),
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Colors.green,
        accentColor: Color(0xFFa7f542)    
        ),
    );
  }
}
