import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  //Variable
  
  int hasil = 0;
  TextEditingController angka1 = new TextEditingController();
  TextEditingController angka2 = new TextEditingController();

  //Fungsi
  tambah(){
    setState(() {
      hasil = int.parse(angka1.text) + int.parse(angka2.text);
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Calculator'),
      ),
      body: ListView(
        children: [
          Container(
            padding: EdgeInsets.all(10.0),
            child: TextField(
              keyboardType: TextInputType.number,
              //ini setting variable disini (angka1)
              controller: angka1,
              decoration:InputDecoration(labelText: "Masukkan Angka Pertama")
            )
          ),
          Container(
            padding: EdgeInsets.all(10.0),
            child: TextField(
              //ini setting variable disini (angka2)              
              controller: angka2,
              decoration:InputDecoration(labelText: "Masukkan Angka Kedua")
            )
          ),
          Container(
            padding: EdgeInsets.all(10.0),
            child: FlatButton(color: Colors.orange,
            onPressed: (){
              tambah();
            }, 
            child: Text("Tambah",
            style: TextStyle(color: Colors.white),
              ),
            ),
          ),
          Center(child: Text(hasil.toString(), style: TextStyle(fontSize: 30.0),),),
        ],
      ),
      
    );
  }
}