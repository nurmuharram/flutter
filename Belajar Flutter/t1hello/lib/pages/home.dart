import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Hello World'),
        ),
        body: Text('Kiban rakan...?',
            textAlign: TextAlign.center,
            style: TextStyle(color: Color(0xFF000000))));
  }
}
