import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

class Detail extends StatefulWidget {
  @override
  _DetailState createState() => _DetailState();
}

class _DetailState extends State<Detail> {
  TextEditingController namaController = new TextEditingController();
  TextEditingController alamatController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();

  postData(){
    var jsonData = {
      "name": namaController.text,
      "address": alamatController.text,
      "email": emailController.text,
    };

    var dataJson = json.encode(jsonData);
    print(dataJson);

    http.post("https://script.google.com/macros/s/AKfycbyb7fjSjSdviltbeDkGCaqzVdUbayoTBToPy5yItN3BKrcPI8J-/exec",body: dataJson)
    .then((response){
//response sukes
print("Data berhasil");
      })
      .catchError((err){
// response error
print("Error"+err);
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Tambah Data"),
      ),
      body: Container(
        padding: EdgeInsets.all(10.0),
        child: ListView(
          children:[
            TextField(
              controller: namaController,
              decoration: InputDecoration(labelText: "Nama")
            ),
            TextField(
              controller: alamatController,
              decoration: InputDecoration(labelText: "Alamat")
            ),
            TextField(
              controller: emailController,
              decoration: InputDecoration(labelText: "Email")
            ),
            FlatButton(onPressed: (){
              postData();
            }, 
            child: Text("Tambah Data", style: TextStyle(color: Colors.white),),
            color: Colors.orange,)            
          ]
        )
      ),
      
    );
  }
}