import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Grid'),),
      body: Container(
        padding: EdgeInsets.all(50.0),
        child: ListView(
          children: [
            Text("Kiban rakan.."),
            Column(
              children: [
                Text("kolom 1"),
                Text('kolom 2'),
                Text('kolom 2'),
              ],
            ),
            Row(
              children:[
                Text('kolom 2'),
                Text('kolom 2'),
              ]
            ),
            Row(
              children:[
                Expanded(child: Text('Expanded text nih!')),
                Text("Ni text biasa")
              ]
            )
          ],
        )
      ),
    );
  }
}