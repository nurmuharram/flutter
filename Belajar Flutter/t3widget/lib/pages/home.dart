import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Widget'),),
      body: Container(
        padding: EdgeInsets.all(10.0),
        child: ListView(
          children: [
            Text("Di atass nii tess yaaa"),
            TextField(
              decoration: InputDecoration(labelText: "Nama Anda"),),
            Image.network("https://asset.kompas.com/crops/GOGE5UVQJ-kX5EkpxiXgGTRXt_E=/19x61:898x647/750x500/data/photo/2020/11/20/5fb73ab6933c4.jpg"),
            Image.asset("images/1.jpg"),
            // ignore: deprecated_member_use
            RaisedButton(
              child:Text("Klik disini"),
              color: Colors.blue, 
              onPressed: () { 
                print("halooo");
               },),
            Padding(padding: EdgeInsets.all(5.0),),
            // ignore: deprecated_member_use
            FlatButton(
              child:Text("Klik disini"),
              color: Colors.blue, 
              onPressed: () { 
                print("halooo");
               },),
            ListTile(
              leading: Icon(Icons.favorite),
              title: Text("Judul ni!"),
              subtitle: Text("Sub ni"),
              trailing: Icon(Icons.keyboard_arrow_right_outlined
            )),
            Card(
              child:ListTile(
              leading: Icon(Icons.favorite),
              title: Text("Judul ni!"),
              subtitle: Text("Sub ni"),
              trailing: Icon(Icons.keyboard_arrow_right_outlined
            )),),
            Row(
              children: [
              Expanded(
                child: Card(
                  child:Column(
                    children:[
                      Image.network('https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Vertical/12.jpg'),
                      Text("Denim Shirt"),
                      Text("100.000"),
                      FlatButton(
                        onPressed: (){},
                        child: Row(children: [
                          Icon(Icons.shopping_cart),
                          Text("Beli Sekarang")
                        ],) 
                        )
                    ]
                  ))),
              Expanded(
                child: Card(
                  child:Column(
                    children:[
                      Image.network("https://asset.kompas.com/crops/GOGE5UVQJ-kX5EkpxiXgGTRXt_E=/19x61:898x647/750x500/data/photo/2020/11/20/5fb73ab6933c4.jpg"),
                      Text("Denim Shirt"),
                      Text("100.000"),
                      FlatButton(
                        onPressed: (){},
                        child: Row(children: [
                          Icon(Icons.shopping_cart),
                          Text("Beli Sekarang")
                        ],) 
                        )
                    ]
                  )))
            ],)
            
          ],
        ),
      ),
    );
  }
}