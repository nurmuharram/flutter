import 'package:flutter/material.dart';

class Detail extends StatefulWidget {
  Detail({this.result,this.nama});
  final int result;
  final String nama;
  @override
  _DetailState createState() => _DetailState();
}

class _DetailState extends State<Detail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Result"),),
      body: Container(
        padding: EdgeInsets.all(10.0),
        child: Text(widget.result.toString()+"-------"+widget.nama),

      )      
    );
  }
}