
import 'package:flutter/material.dart';
import 'package:t6navigationdata/home.dart/detail.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
int hasil = 0;
TextEditingController angkaPertama = new TextEditingController();
TextEditingController angkaKedua = new TextEditingController();

//fungsi

tambah(){
  hasil = int.parse(angkaPertama.text) + int.parse(angkaKedua.text);
  print(hasil.toString());
  Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context)=>Detail(result: hasil,nama:"Mumu",)));
}
  


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Calculator V2'),),
      body: ListView(
        children:[
          Container(
            padding: EdgeInsets.all(10.0),
            child: TextField(
              controller: angkaPertama,
              decoration: InputDecoration(labelText: "Masukkan Angka Pertama"),
            ),
          ),
           Container(
            padding: EdgeInsets.all(10.0),
            child: TextField(
              controller: angkaKedua,
              decoration: InputDecoration(labelText: "Masukkan Angka Kedua"),              
            ),),
            Container(
            padding: EdgeInsets.all(10.0),
            child: FlatButton(
              onPressed: (){
                tambah();
              },
              color: Colors.orange,
              child: Text("Tambah",style: TextStyle(color: Colors.white),),
            ),
          )
        ]),
    );
  }
}